<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 10.07.20
 * Time: 17:48
 */

use \core\Router;

spl_autoload_register(function ($class_name){
	$class = str_replace('\\', '/', $class_name) . '.php';
	if(file_exists($class)) {
		require_once( $class );
	}else
	{
		Router::ErrorPage404();
	}
});

session_start();
Router::start();



function writeToLog ($data) {
	$log = "\n-------------------\n";
	$log .= print_r($data, 1);
	$log .= "\n------------------\n";

	file_put_contents('log/log.txt', $log, FILE_APPEND);
}
